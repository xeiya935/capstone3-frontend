import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';
import {Button} from 'reactstrap';

const CheckoutForm = (props) => {

	let course = props.course

	const handlePayAndBookClass = async (e) => {

		let {token} = await props.stripe.createToken({name:"Name"})

		let price = course.price*100;
		let description = "Payment for " + course.language + " class from Codebase.com";

		let response = await axios.post('https://fast-ocean-11047.herokuapp.com/charge', {
			headers: {"Content-type":"text-plain"},
			body: token.id,
			amount: price,
			description: description
		})

		if(response.ok){
			console.log("Purchase Complete");
		}
		props.handleCompleteBooking()
	}

	return (
		<div className="checkout">
			<p>Amount: {course.price} PHP</p>
        	<p>Would you like to complete the purchase for {course.language} class?</p>
        	<CardElement />
        	<div className="d-flex justify-content-center my-3">
	        	<Button
	        		color="success"
	        		className="mx-1"
	        		onClick={handlePayAndBookClass}
	        		disabled={props.time==="" || props.date==="" ? true : false}
	        	>

	        		Book Now
	        	</Button>
	        	<Button
					color="danger"
					className="mx-1"
					onClick={props.handleCancelBooking}
				>
					Cancel
				</Button>
			</div>
      	</div>
	)
}

export default injectStripe(CheckoutForm);