import React from 'react';
import DayPicker from 'react-day-picker';

const Calendar = (props) => {

	const handleDayClick = (day) => {
		props.handleDateInput(day)
	}
	
	const past = {
		before: new Date()
	}

	return(
		<React.Fragment>
			<div className="d-flex justify-content-center">
				<DayPicker 
					onDayClick={handleDayClick}
					showOutsideDays
					disabledDays={past}
				/>
			</div>
		</React.Fragment>
	)
}

export default Calendar;