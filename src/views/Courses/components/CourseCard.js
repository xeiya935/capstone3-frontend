import React, {useState} from 'react';
import {Button} from 'reactstrap'
import Image from 'react-bootstrap/Image'
import BookingForm from './BookingForm'
import UpdateCourse from './UpdateCourse'

const CourseCard = (props) => {

	const course = props.course
	const [showBookingForm, setShowBookingForm] = useState(false);

	const handleShowBookingForm = () => {
		setShowBookingForm(!showBookingForm);
	}

	const handleCourseState = () => {
		handleShowBookingForm();
		props.handleCourseInput(course.language);
	}

	const handleUnregisteredUser = () => {
		window.location.replace('#/register')
	}

	const handleUpdateCourseState = () => {
		props.handleShowUpdateCourseForm();
		props.handleCourseInput(course.language);
		props.handleDescriptionInput(course.description)
		props.handlePriceInput(course.price)
	}

	return(
		<React.Fragment>
			<div className="card col-lg-3 col-md-3 py-3 mx-3 my-3 text-center">
				<h2>{course.language}</h2>
				<p>{course.description}</p>
				<Image 
					src={process.env.PUBLIC_URL + course.courseImage}
					thumbnail
				/>
				<h3>{course.price} PHP</h3>
				{props.user.role === "User" ?
				<Button
					// color="success"
					className="my-1 btnsuccess"
					onClick={handleCourseState}
				>Book Class</Button>
				: ""}
				{!sessionStorage.token ?
				<Button
					className="btnsuccess"
					onClick={handleUnregisteredUser}
				>BookClass</Button>
				: ""}
				{props.user.role === "Admin" ?
					<React.Fragment>
						<Button
							className="my-1 btndanger"
							onClick={()=>props.handleDeleteCourse(course._id)}
						>Delete Course</Button>
						<Button
							// color="info"
							className="my-1 btninfo"
							onClick={handleUpdateCourseState}
						>
						Update Course</Button>
					</React.Fragment>
				: ""}
			<BookingForm 
				showBookingForm={showBookingForm}
				handleShowBookingForm={handleShowBookingForm}
				course={course}
				handleTimeInput={props.handleTimeInput}
				handleDateInput={props.handleDateInput}
				handleSaveBooking={props.handleSaveBooking}
				time={props.time}
				date={props.date}
				handleRefresh={props.handleRefresh}
				handleSaveCourse={props.handleSaveCourse}
			/>
			<UpdateCourse
				showUpdateCourseForm={props.showUpdateCourseForm}
				handleShowUpdateCourseForm={props.handleShowUpdateCourseForm}
				course={course}
				handleLanguageChange={props.handleLanguageChange}
				handleCourseImageChange={props.handleCourseImageChange}
				handleDescriptionChange={props.handleDescriptionChange}
				handlePriceChange={props.handlePriceChange}
				handleRefresh={props.handleRefresh}
			/>
			</div>
		</React.Fragment>
	)
}

export default CourseCard;