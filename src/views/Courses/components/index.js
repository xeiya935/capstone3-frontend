import CourseCard from './CourseCard';
import AddCourse from './AddCourse';

export {
	CourseCard,
	AddCourse
}