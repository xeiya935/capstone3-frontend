import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';

const AddCourse = (props) => {

	const handleAddCourse = () => {
		props.handleSaveCourse();
		props.handleRefresh();
		props.handleShowCourseForm();
	}

	const handleCancelAddCourse = () => {
		props.handleRefresh();
		props.handleShowCourseForm();
	}

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showCourseForm}
				toggle={props.handleShowCourseForm}
			>
				<ModalHeader
					toggle={props.handleShowCourseForm}
				>
					Add a new course
				</ModalHeader>
				<ModalBody>
					<FormInput 
				        label={"Language"}
				        type={"string"}
				        name={"language"}
				        placeholder={"Enter course name"}
				        onBlur={(e)=>props.handleLanguageChange(e)}
			        />
			        <FormInput 
				        label={"Course Image"}
				        type={"file"}
				        name={"courseImage"}
				        onChange={props.selectImage}
			        />
			        <small>{props.infoMessage}</small>
			        <FormInput 
				        label={"Description"}
				        type={"textarea"}
				        name={"description"}
				        placeholder={"Enter course description here"}
				        onBlur={(e)=>props.handleDescriptionChange(e)}
			        />
			        <FormInput 
				        label={"Price (PHP)"}
				        type={"number"}
				        name={"price"}
				        placeholder={"Enter course price here"}
				        onBlur={(e)=>props.handlePriceChange(e)}
			        />
			        <div className="d-flex justify-content-center">
			        
					<Button
						className="mx-1 btnsuccess"
						onClick={handleAddCourse}
					>
						Add Course
					</Button>
					<Button
						// color="danger"
						className="mx-1 btndanger"
						onClick={handleCancelAddCourse}
					>Cancel</Button>
					</div>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default AddCourse;