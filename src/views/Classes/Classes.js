import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {ClassRow} from './components'
import moment from 'moment';
import {NavigationBar, Footer} from '../Layout'

const Classes = () => {

	const [classes, setClasses] = useState([])
	const [time, setTime] = useState("");
	const [date,setDate] = useState("");
	const [classSchedule, setClassSchedule] = useState("");

	const handleRefresh = () => {
		setTime("");
		setDate("");
		setClassSchedule("");
	}

	const handleDeleteClass = (classId) => {
		axios.delete('https://fast-ocean-11047.herokuapp.com/deleteClass/'+classId).then(res=>{
			let index = classes.findIndex(classSchedule=>classSchedule._id===classId);
			let newClasses = [...classes];
			newClasses.splice(index,1);
			setClasses(newClasses);
		})
	}

	const handleTimeInput = (time) => {
		setTime(time);
		console.log(time);
	}

	const handleDateInput = (date) => {
		setDate(date);
		console.log(date);
	}

	const handleUpdateBooking = () => {
		axios.patch('https://fast-ocean-11047.herokuapp.com/userUpdateClass/'+ classSchedule, {
			time: time,
			date: moment(date).format("MMM DD YYYY")
		}).then(res=>{
			let index = classes.findIndex(classId=>classId._id === classSchedule)
			let newClasses = [...classes];
			newClasses.splice(index,1,res.data)
			setClasses(newClasses);
			handleRefresh();
		})
	}

	const handleClassScheduleInput = (classSchedule) => {
		let updatedClass = classSchedule;
		setClassSchedule(updatedClass);
		console.log(classSchedule)
	}

	const [user, setUser] = useState("")

	useEffect(()=>{

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			console.log(sessionStorage.user)
			if(user.role === "Admin"){
				axios.get('https://fast-ocean-11047.herokuapp.com/showClasses').then(res=>{
					setClasses(res.data);
				})
			}else{
				let student = user.firstName + " " + user.lastName
				console.log(student)
				axios.get('https://fast-ocean-11047.herokuapp.com/showClassesByUser/'+student).then(res=>{
					setClasses(res.data)
					console.log(res.data)
				})
			}
			setUser(user);

		}else{
			window.location.replace('#/login')
		}

	}, []);

	return(
		<React.Fragment>
			<NavigationBar 
				user={user}
			/>
			<h1 className="text-center pt-5 header-text">My Classes</h1>
			<div className="col-lg-10 offset-lg-1">
				<table className="table table-striped border text-center">
					<thead className="table-header">
						<tr>
							<th>ClassCode</th>
							<th>Class</th>
							<th>Date</th>
							<th>Time</th>
							<th>Tutor</th>
							{user.role==="User" ? "" :
							<th>Student</th>
							}
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{classes.map(classSchedule=>
							<ClassRow 
								key={classSchedule._id}
								classSchedule={classSchedule}
								handleDeleteClass={handleDeleteClass}
								handleTimeInput={handleTimeInput}
								handleDateInput={handleDateInput}
								handleUpdateBooking={handleUpdateBooking}
								handleClassScheduleInput={handleClassScheduleInput}
								user={user}
							/>
						)}
					</tbody>
				</table>
			</div>
			<Footer />
		</React.Fragment>
	)
}

export default Classes;