import React, {useState} from 'react';
import {Button} from 'reactstrap';
import UpdateBooking from './UpdateBooking';

const ClassRow = (props) => {

	const classSchedule = props.classSchedule;
	const [showUpdateForm, setShowUpdateForm] = useState(false);

	const handleShowUpdateClassForm = () => {
		setShowUpdateForm(!showUpdateForm);
		console.log(showUpdateForm)
	}

	const handleClassScheduleState = () => {
		handleShowUpdateClassForm()
		props.handleClassScheduleInput(classSchedule._id);
		props.handleDateInput(classSchedule.date);
		props.handleTimeInput(classSchedule.time);
	}

	return (
		<React.Fragment>
			<UpdateBooking 
				classSchedule={classSchedule}
				showUpdateForm={showUpdateForm}
				handleShowUpdateClassForm={handleShowUpdateClassForm}
				handleTimeInput={props.handleTimeInput}
				handleDateInput={props.handleDateInput}
				handleUpdateBooking={props.handleUpdateBooking}
			/>
			<tr className="table-body">
				<td>{classSchedule.classCode}</td>
				<td>{classSchedule.language}</td>
				<td>{classSchedule.date}</td>
				<td>{classSchedule.time}</td>
				<td>{classSchedule.tutor}</td>
				{props.user.role==="User" ? "" :
				<td>{classSchedule.student}</td>
				}
				<td>{classSchedule.bookingStatus}</td>
				<td>
					<Button
						// color="info"
						className="mx-1 py-1 btninfo"
						onClick={handleClassScheduleState}
					>Update Class</Button>
					{props.user.role==="User" ? "" :
						<Button
							// color="danger"
							className="mx-1 py-1 btndanger"
							onClick={()=>props.handleDeleteClass(classSchedule._id)}
						>Delete Class</Button>
					}
				</td>
			</tr>
		</React.Fragment>
	)
}

export default ClassRow;