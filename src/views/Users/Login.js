import React, { useState } from "react";
import { FormInput } from "../../globalcomponents";
import { Button } from "reactstrap";
import axios from "axios";
import {NavigationBar, Footer} from '../Layout'

const Login = () => {
  
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    const handleEmailChange = e =>{
        setEmail(e.target.value)
        console.log(e.target.value)
    }

    const handlePasswordChange = e =>{
        setPassword(e.target.value)
        console.log(e.target.value)
    }

    const handleLogin = () => {
        axios.post('https://fast-ocean-11047.herokuapp.com/login',{
            email,
            password
        }).then(res=>{
          sessionStorage.token = res.data.token;
          sessionStorage.user = JSON.stringify(res.data.user);
          console.log(res.data.token);

          window.location.replace('#/courses');
        });
    }

  return (
    <React.Fragment>
      
      <NavigationBar />
      <div className="min-vh-100">
      <h1 className="text-center pt-5 header-text">Login</h1>
      <div className="col-lg-4 offset-lg-4 pb-3 col-md-4 offset-md-4">
        <FormInput
          label={"Email"}
          placeholder={"Enter Your Email"}
          type={"email"}
          onChange={handleEmailChange}
        />
        <FormInput
          label={"Password"}
          placeholder={"Enter Your Password"}
          type={"password"}
          onChange={handlePasswordChange}
        />
        <Button 
            block 
            color="info"
            className="btnsuccess"
            onClick={handleLogin}
            >
          Login
        </Button>
      </div>
      </div>
      <Footer />

    </React.Fragment>
  );
};

export default Login;